### Issue
<!-- Please, describe the issue here -->



### Steps for reproducing the issue
<!-- Step, to reproduce it -->


<!-- If you know the version of OpenMultiMap that you are using (can be found in about page) -->
Version of OpenMultiMap:


<!-- Your Android version -->
Android version:

