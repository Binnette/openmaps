# OpenMultiMaps [![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Crowdin](https://badges.crowdin.net/openmaps/localized.svg)](https://crowdin.com/project/openmaps) [![pipeline status](https://framagit.org/tom79/openmaps/badges/develop/pipeline.svg)](https://framagit.org/tom79/openmaps/commits/develop)

A simple client to display maps from [OpenStreetMap](https://www.openstreetmap.org).

Issue tracker: [framagit.org/tom79/openmaps/issues](https://framagit.org/tom79/openmaps/issues)

[<img alt='Get it on F-Droid' src='./img/get-it-on-fdroid.png' height="80"/>](https://f-droid.org/packages/app.fedilab.openmaps/)


[Releases](https://framagit.org/tom79/openmaps/-/tags)

<img src="./img/img1.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./img/img2.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./img/img3.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./img/img4.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

Idea from [@Bristow_69@framapiaf.org](https://framapiaf.org/@Bristow_69)

Translations are done with [Crowdin](https://crowdin.com/project/openmaps), if a language is missing, please open an issue.