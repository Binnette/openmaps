package app.fedilab.openmaps;
/* Copyright 2019 Thomas Schneider
 *
 * This file is a part of OpenMultiMaps
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * OpenMultiMaps is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with OpenMultiMaps; if not,
 * see <http://www.gnu.org/licenses>. */
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import com.franmontiel.localechanger.LocaleChanger;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.OnMenuItemClickListener;
import com.skydoves.powermenu.PowerMenu;
import com.skydoves.powermenu.PowerMenuItem;
import java.util.Locale;
import app.fedilab.openmaps.helper.Helper;

public class AboutActivity  extends AppCompatActivity {


    private PowerMenu powerMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView about_version = findViewById(R.id.about_version);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            about_version.setText(getResources().getString(R.string.about_vesrion, version));
        } catch (PackageManager.NameNotFoundException ignored) {}

        setTitle(R.string.about_the_app);
        if( getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        //Developer click for Mastodon account
        TextView developer_mastodon = findViewById(R.id.developer_mastodon);
        SpannableString content = new SpannableString(developer_mastodon.getText().toString());
        content.setSpan(new ForegroundColorSpan(ContextCompat.getColor(AboutActivity.this,R.color.colorAccent)), 0, content.length(), 0);
        developer_mastodon.setText(content);
        developer_mastodon.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://framapiaf.org/@fedilab"));
            startActivity(browserIntent);
        });

        //Developer Github
        TextView github = findViewById(R.id.github);
        content = new SpannableString(github.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        github.setText(content);
        github.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/stom79"));
            startActivity(browserIntent);
        });

        //Developer Framagit
        TextView framagit = findViewById(R.id.framagit);
        content = new SpannableString(framagit.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        framagit.setText(content);
        framagit.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://framagit.org/tom79"));
            startActivity(browserIntent);
        });

        //Developer Codeberg
        TextView codeberg = findViewById(R.id.codeberg);
        content = new SpannableString(codeberg.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        codeberg.setText(content);
        codeberg.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://codeberg.org/tom79"));
            startActivity(browserIntent);
        });

        //Developer donation
        TextView developer_donation = findViewById(R.id.developer_donation);
        content = new SpannableString(developer_donation.getText().toString());
        content.setSpan(new ForegroundColorSpan(ContextCompat.getColor(AboutActivity.this,R.color.colorAccent)), 0, content.length(), 0);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        developer_donation.setText(content);
        developer_donation.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://fedilab.app/page/donations/"));
            startActivity(browserIntent);
        });


        //Idea click for Mastodon account
        TextView bristow_69_mastodon = findViewById(R.id.bristow_69_mastodon);
        content = new SpannableString(bristow_69_mastodon.getText().toString());
        content.setSpan(new ForegroundColorSpan(ContextCompat.getColor(AboutActivity.this,R.color.colorAccent)), 0, content.length(), 0);
        bristow_69_mastodon.setText(content);
        bristow_69_mastodon.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://framapiaf.org/@Bristow_69"));
            startActivity(browserIntent);
        });

        TextView license = findViewById(R.id.license);
        content = new SpannableString(license.getText().toString());
        content.setSpan(new ForegroundColorSpan(ContextCompat.getColor(AboutActivity.this,R.color.colorAccent)), 0, content.length(), 0);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        license.setText(content);
        license.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.gnu.org/licenses/quick-guide-gplv3.fr.html"));
            startActivity(browserIntent);
        });


        ImageButton action_language = findViewById(R.id.action_language);
        action_language.setOnClickListener(this::createLanguageMenu);

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleChanger.onConfigurationChanged();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }


    public void createLanguageMenu(View action_language){
        SharedPreferences sharedpref = getSharedPreferences(Helper.APP_SHARED_PREF, MODE_PRIVATE);
        String defaultLocale = sharedpref.getString(Helper.SET_DEFAULT_LOCALE_NEW, Locale.getDefault().getLanguage());
        powerMenu = new PowerMenu.Builder(AboutActivity.this)
                .addItem(new PowerMenuItem(getString(R.string.english), defaultLocale.compareTo("en") == 0))
                .addItem(new PowerMenuItem(getString(R.string.french), defaultLocale.compareTo("fr") == 0))
                .addItem(new PowerMenuItem(getString(R.string.german), defaultLocale.compareTo("de") == 0))
                .addItem(new PowerMenuItem(getString(R.string.basque_), defaultLocale.compareTo("eu") == 0))
                .addItem(new PowerMenuItem(getString(R.string.occitan_), defaultLocale.compareTo("oc") == 0))
                .addItem(new PowerMenuItem(getString(R.string.spanish), defaultLocale.compareTo("es") == 0))
                .addItem(new PowerMenuItem(getString(R.string.portuguese), defaultLocale.compareTo("pt") == 0))
                .addItem(new PowerMenuItem(getString(R.string.dutch), defaultLocale.compareTo("nl") == 0))
                .addItem(new PowerMenuItem(getString(R.string.hungarian), defaultLocale.compareTo("hu") == 0))
                .addItem(new PowerMenuItem(getString(R.string.swedish), defaultLocale.compareTo("sv") == 0))
                .addItem(new PowerMenuItem(getString(R.string.traditional_chinese), defaultLocale.compareTo("zh-TW") == 0))
                .setAnimation(MenuAnimation.SHOWUP_TOP_LEFT)
                .setMenuRadius(10f)
                .setMenuShadow(10f)
                .setTextColor(ContextCompat.getColor(AboutActivity.this, R.color.black))
                .setTextGravity(Gravity.CENTER)
                .setSelectedTextColor(Color.WHITE)
                .setMenuColor(Color.WHITE)
                .setSelectedMenuColor(ContextCompat.getColor(AboutActivity.this, R.color.colorPrimary))
                .setOnMenuItemClickListener(onMenuLanguageListener)
                .build();
        powerMenu.showAsDropDown(action_language);
    }


    private OnMenuItemClickListener<PowerMenuItem> onMenuLanguageListener = new OnMenuItemClickListener<PowerMenuItem>() {
        @Override
        public void onItemClick(int position, PowerMenuItem item) {
            SharedPreferences sharedpref = getSharedPreferences(Helper.APP_SHARED_PREF, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpref.edit();
            String stringLocal = "en";
            switch (position) {
                case 0:
                    stringLocal = "en";
                    break;
                case 1:
                    stringLocal = "fr";
                    break;
                case 2:
                    stringLocal = "de";
                    break;
                case 3:
                    stringLocal = "eu";
                    break;
                case 4:
                    stringLocal = "oc";
                    break;
                case 5:
                    stringLocal = "es";
                    break;
                case 6:
                    stringLocal = "pt";
                    break;
                case 7:
                    stringLocal = "nl";
                    break;
                case 8:
                    stringLocal = "hu";
                    break;
                case 9:
                    stringLocal = "sv";
                    break;
                case 10:
                    stringLocal = "zh-TW";
                    break;
            }
            editor.putString(Helper.SET_DEFAULT_LOCALE_NEW, stringLocal);
            powerMenu.dismiss();
            editor.apply();
            LocaleChanger.setLocale(new Locale(stringLocal));

            Intent i = getBaseContext().getPackageManager().
                    getLaunchIntentForPackage(getBaseContext().getPackageName());
            assert i != null;
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    };

}
